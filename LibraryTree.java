import java.util.ArrayList;
public class LibraryTree {

	
	public LibraryNode primaryRoot;		//root of the primary B+ tree
	public LibraryNode secondaryRoot;	//root of the secondary B+ tree

	public LibraryTree(Integer order)
	{
		LibraryNode.order = order;
		primaryRoot = new LibraryNodeLeaf(null);
		primaryRoot.level = 0;
		secondaryRoot = new LibraryNodeLeaf(null);
		secondaryRoot.level = 0;
	}
	
	public void addBook(CengBook book) {

		// add methods to fill both primary and secondary tree
		this.addToFirst(primaryRoot,book);
		//this.addToSecond(secondaryRoot,book);
	}
	
	
	
	public CengBook searchBook(Integer key) {
		
		// add methods to find the book with the searched key in primary B+ tree
		// return value will not be tested, just prInteger as the specicifications say
		LibraryNode root = primaryRoot; 
		Boolean found = false;

		while(!found)
		{
			if(root instanceof LibraryNodePrimaryIndex)
			{
				System.out.println("<index>");
				LibraryNodePrimaryIndex primNode = (LibraryNodePrimaryIndex) root;
				for(int i = 0;i < primNode.keyCount();i++)
				{
					System.out.println(primNode.keyAtIndex(i));
					if(i == 0 && primNode.keyAtIndex(i) > key)
					{
						root = primNode.getChildrenAt(0);
					}
					else if(i == primNode.keyCount()-1 && primNode.keyAtIndex(primNode.keyCount()-1) < key)
					{
						root = primNode.getChildrenAt(primNode.keyCount());
					}
					else if(primNode.keyAtIndex(i).equals(key))
					{
						root = primNode.getChildrenAt(i+1);
					}
					else if(i != 0 && primNode.keyAtIndex(i-1) < key && primNode.keyAtIndex(i) > key)
					{
						root = primNode.getChildrenAt(i);
					}
				}
				System.out.println("</index>");

			}
			else
			{
				LibraryNodeLeaf leafNode = (LibraryNodeLeaf) root;
				System.out.println("<data>");
				for(CengBook data : leafNode.getbooks())
				{
					System.out.println(data.fullName());
					if(data.key().equals(key))
					{
						break;
					}
				}
				found = true;
				System.out.println("</data>");

			}
		}
		return null;
	}
	
	
	public void printPrimaryLibrary() {

		ArrayList<LibraryNode> queue = new ArrayList<LibraryNode>();
		queue.add(primaryRoot);
		while(!queue.isEmpty()) 
		{
			LibraryNode head = queue.get(0);
			queue.remove(0);
			if(head instanceof LibraryNodeLeaf)
			{
				LibraryNodeLeaf leafHead = (LibraryNodeLeaf)head;
				for(Integer i = 0;i <leafHead.bookCount();i++)
				{
					System.out.println(leafHead.bookAtIndex(i).fullName());
				}
			}
			else
			{
				LibraryNodePrimaryIndex priHead = (LibraryNodePrimaryIndex)head;
				for(LibraryNode child : priHead.getAllChildren())
				{
					queue.add(queue.size(),child);
				}
			}

		}
	}
	
	public void printSecondaryLibrary() {
		
		// add methods to prInteger the secondary B+ tree in Depth-first order
		
	}
	
	// Extra functions if needed


	private void addToFirst(LibraryNode root,CengBook book)
	{
		Integer key = book.key();
		if(root instanceof LibraryNodeLeaf)
		{
			LibraryNodeLeaf leafRoot = (LibraryNodeLeaf)root;
			insertInPlace(leafRoot, book);
			Integer newBookCount = leafRoot.bookCount();
			Integer rootOrder  = LibraryNode.order;
			if(newBookCount.equals( 2*rootOrder+ 1))
			{
				LibraryNodeLeaf leftSide = new LibraryNodeLeaf(root.getParent());
				LibraryNodeLeaf rightSide = new LibraryNodeLeaf(root.getParent());
				Integer leftSideSize = newBookCount/2;

				for(Integer i = 0;i<leftSideSize;i++)
				{
					leftSide.addBook(i,leafRoot.bookAtIndex(i));
				}
				for(Integer i = 0; i < leftSideSize+1;i++)
				{
					rightSide.addBook(i,leafRoot.bookAtIndex(i+leftSideSize));
				}
				Integer copyUpIndex = rightSide.bookKeyAtIndex(0);
				if(root == primaryRoot)
				{
					primaryRoot = new LibraryNodePrimaryIndex(null);
					leftSide.setParent(primaryRoot);
					rightSide.setParent(primaryRoot);
				}
				insertInPlace((LibraryNodePrimaryIndex) rightSide.getParent(), copyUpIndex, leftSide, rightSide);
			}
			else
			{
				return;
			}

		}
		else
		{
			Integer rootOrder = LibraryNode.order;
			LibraryNodePrimaryIndex priRoot= (LibraryNodePrimaryIndex) root;
			if(priRoot.keyCount().equals(1))
			{
				if(priRoot.keyAtIndex(0) <= key)
				{
					this.addToFirst(priRoot.getChildrenAt(1),book);
				}
				else
				{
					this.addToFirst(priRoot.getChildrenAt(0),book);
				}
			}
			else if(priRoot.keyAtIndex(0) > key)
			{
				this.addToFirst(priRoot.getChildrenAt(0),book);
			}
			else if(priRoot.keyAtIndex(priRoot.keyCount()-1) < key)
			{
				this.addToFirst(priRoot.getChildrenAt(priRoot.keyCount()),book);
			}
			else
			{
				for(int i = 0; i < priRoot.keyCount()-1;i++)
				{
					if(priRoot.keyAtIndex(i) < key && priRoot.keyAtIndex(i+1) > key)
					{
						this.addToFirst(priRoot.getChildrenAt(i+1),book);
						break;
					}
				}
			}

			if(priRoot.keyCount().equals(2*rootOrder+1))
			{
				
				LibraryNodePrimaryIndex leftSide = new LibraryNodePrimaryIndex(root.getParent());
				LibraryNodePrimaryIndex rightSide = new LibraryNodePrimaryIndex(root.getParent());
				Integer leftSideSize = rootOrder;

				for(Integer i = 0;i<leftSideSize;i++)
				{
					leftSide.addKey(i,priRoot.keyAtIndex(i));
					leftSide.addChild(i,priRoot.getChildrenAt(i));
					priRoot.getChildrenAt(i).setParent(leftSide);
				}
				leftSide.addChild(leftSideSize,priRoot.getChildrenAt(leftSideSize));
				priRoot.getChildrenAt(leftSideSize).setParent(leftSide);

				for(Integer i = 0; i < leftSideSize;i++)
				{
					rightSide.addKey(i,priRoot.keyAtIndex(i+leftSideSize+1));
					rightSide.addChild(i,priRoot.getChildrenAt(i+leftSideSize+1));
					priRoot.getChildrenAt(i+leftSideSize+1).setParent(rightSide);

				}
				rightSide.addChild(leftSideSize,priRoot.getChildrenAt(2*leftSideSize+1));
				priRoot.getChildrenAt(2*leftSideSize+1).setParent(rightSide);

				Integer pushUpIndex = priRoot.keyAtIndex(rootOrder);
				if(root == primaryRoot)
				{
					LibraryNodePrimaryIndex newRoot = new LibraryNodePrimaryIndex(null);
					primaryRoot = newRoot;
					rightSide.setParent(newRoot);
					leftSide.setParent(newRoot);

				}
				insertInPlace((LibraryNodePrimaryIndex)rightSide.getParent(), pushUpIndex, leftSide, rightSide);

			}
			else
			{
				return;
			}
		}
	}

	private void addToSecond(LibraryNode root,CengBook book)
	{

	}

	private void insertInPlace(LibraryNodeLeaf root,CengBook book)
	{
		Integer bookCount = root.bookCount();
		if(bookCount.equals(1) || bookCount.equals(0))
		{
			if(bookCount.equals(0))
			{
				primaryRoot.level = 0;
				root.addBook(0,book);
			}
			if(bookCount.equals(1))
			{
				if(root.bookKeyAtIndex(0) < book.key())
				{
					root.addBook(1,book);
				}
				else if(root.bookKeyAtIndex(0) > book.key())
				{
					root.addBook(0,book);
				}
				else
				{
					return;
				}
			}
		}
		else if (root.bookKeyAtIndex(0) > book.key())
		{
			root.addBook(0,book);
		}
		else{
			for(Integer i = 0 ; i < bookCount;i++)
			{
				if(root.bookKeyAtIndex(i).equals(book.key()))
				{
					return;
				}
				else if(i.equals(bookCount-1))
				{
					root.addBook(bookCount,book);
				}
				else if(root.bookKeyAtIndex(i) < book.key() && root.bookKeyAtIndex(i+1) > book.key())
				{
					root.addBook(i+1,book);
				}
			}
		}
	}
	
	private void insertInPlace(LibraryNodePrimaryIndex root,Integer key,LibraryNode leftSide,LibraryNode rightSide)
	{
		Integer keyCount = root.keyCount();
		if(keyCount.equals(0))
		{
			root.addKey(0,key);
			root.addChild(0, leftSide);
			root.addChild(1, rightSide);
			return;
		}
		else if(keyCount.equals(1))
		{
			if(key < root.keyAtIndex(0))
			{
				root.addKey(0,key);
				root.addChild(0,leftSide);
				root.setChild(1,rightSide);
			}
			else
			{
				root.addKey(1,key);
				root.setChild(1,leftSide);
				root.addChild(2,rightSide);
			}
			return;
		}
		else if(key < root.keyAtIndex(0))
		{
			root.addKey(0,key);
			root.addChild(0,leftSide);
			root.setChild(1,rightSide);
		}
		else if(key > root.keyAtIndex(keyCount-1))
		{
			root.addKey(keyCount,key);
			root.setChild(keyCount,leftSide);
			root.addChild(keyCount+1,rightSide);
			return;
		}
		for(Integer i = 1; i < keyCount ;i++)
		{
			if(root.keyAtIndex(i-1) < key && root.keyAtIndex(i) > key)
			{
				root.addKey(i,key);
				root.setChild(i, leftSide);
				root.addChild(i+1, rightSide);
				return;
			}
		}
	}
	private void insertInPlace(LibraryNodeSecondaryIndex root,Integer year,Integer key)
	{

	}

	
}
